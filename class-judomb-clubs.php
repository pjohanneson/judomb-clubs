<?php
/**
 * Core class file.
 *
 * @package judomb\clubs
 */

/**
 * Core class.
 *
 * @since 1.0.0 Introduced.
 * @todo Split meta functions to a new class file, since this one's getting unwieldy and a little unfocused.
 */
class JudoMB_Clubs {

	/**
	 * The prefix for our plugin.
	 *
	 * @var string Prefix.
	 * @since 1.0.0 Introduced.
	 */
	const PREFIX = '_judomb_';

	/**
	 * The post type name.
	 *
	 * @var string Post type name.
	 * @since 1.0.0 Introduced.
	 */
	const POST_TYPE = 'judo_club';

	/**
	 * Class constructor.
	 *
	 * @return void
	 * @since 1.0.0 Introduced.
	 */
	function __construct() {
		add_action( 'init',            array( $this, 'judo_club_cpt' ) );
		add_action( 'cmb2_admin_init', array( $this, 'metaboxes' ) );
		add_action( 'admin_notices',   array( $this, 'admin_notices' ) );
		add_action( 'pre_get_posts', array( $this, 'archive_query' ), 10000 );
		add_action( 'wp_enqueue_scripts', array( $this, 'archive_page_styles_and_scripts' ) );

		add_filter( 'the_content', array( $this, 'content_filter' ) );
		add_filter( 'template_include', array( $this, 'template_selector' ) );
	}

	/**
	 * Adds the 'judo_club' custom post type.
	 *
	 * @return void
	 * @since 1.0.0 Introduced.
	 */
	function judo_club_cpt() {

		$labels = array(
			'name'                  => _x( 'Clubs', 'Post Type General Name', 'judomb' ),
			'singular_name'         => _x( 'Club', 'Post Type Singular Name', 'judomb' ),
			'menu_name'             => __( 'Clubs', 'judomb' ),
			'name_admin_bar'        => __( 'Club', 'judomb' ),
			'archives'              => __( 'Club Archives', 'judomb' ),
			'attributes'            => __( 'Club Attributes', 'judomb' ),
			'parent_item_colon'     => __( 'Parent Item:', 'judomb' ),
			'all_items'             => __( 'All Items', 'judomb' ),
			'add_new_item'          => __( 'Add New Item', 'judomb' ),
			'add_new'               => __( 'Add Club', 'judomb' ),
			'new_item'              => __( 'New Club', 'judomb' ),
			'edit_item'             => __( 'Edit Club', 'judomb' ),
			'update_item'           => __( 'Update Club', 'judomb' ),
			'view_item'             => __( 'View Club', 'judomb' ),
			'view_items'            => __( 'View Clubs', 'judomb' ),
			'search_items'          => __( 'Search Club', 'judomb' ),
			'not_found'             => __( 'Not found', 'judomb' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'judomb' ),
			'featured_image'        => __( 'Featured Image', 'judomb' ),
			'set_featured_image'    => __( 'Set featured image', 'judomb' ),
			'remove_featured_image' => __( 'Remove featured image', 'judomb' ),
			'use_featured_image'    => __( 'Use as featured image', 'judomb' ),
			'insert_into_item'      => __( 'Insert into item', 'judomb' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'judomb' ),
			'items_list'            => __( 'Items list', 'judomb' ),
			'items_list_navigation' => __( 'Items list navigation', 'judomb' ),
			'filter_items_list'     => __( 'Filter items list', 'judomb' ),
		);
		$rewrite = array(
			'slug'                  => 'club',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'Club', 'judomb' ),
			'description'           => __( 'Judo Club', 'judomb' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'thumbnail', 'revisions' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 20,
			'menu_icon'             => 'dashicons-groups',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'clubs',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
		);
		register_post_type( JudoMB_Clubs::POST_TYPE, $args );

		// Flush the rewrite rules, if necessary.
		$already_flushed = get_option( JudoMB_Clubs::PREFIX . 'already_flushed', false );
		if ( ! $already_flushed ) {
			flush_rewrite_rules();
			update_option( JudoMB_Clubs::PREFIX . 'already_flushed' , true );
		}
	}

	/**
	 * Creates the metaboxes for the custom post type.
	 *
	 * @return void
	 * @since 1.0.0 Introduced.
	 * @since 1.1.0 Added COVID field.
	 */
	function metaboxes() {
		$prefix = JudoMB_Clubs::PREFIX;

		// Metabox.
		$args = array(
			'id' => $prefix . 'club_meta',
			'title' => 'Club Information',
			'object_types' => array( JudoMB_Clubs::POST_TYPE ),
		);
		$metabox = new_cmb2_box( $args );

		// COVID-19 Return to Play approval level.
		// @since 1.1.0
		$metabox->add_field(
			array(
				'id'      => $prefix . 'covid_rtp_level',
				'name'    => esc_html__( 'COVID-19 RTP Approval Level', 'judomb' ),
				'type'    => 'select',
				'options' => array(
					'0' => 'None',
					'1' => 'Level 1',
					'2' => 'Level 2',
					'3' => 'Level 3',
					'4' => 'Level 4',
				),
			)
		);

		// Club region. @todo Make this load an option, maybe, but that might be overkill.
		$regions = array(
			'East Region'     => 'East Region',
			'West Region'     => 'West Region',
			'South Region'    => 'South Region',
			'North Region'    => 'North Region',
			'Winnipeg Region' => 'Winnipeg Region'
		);
		if ( ! empty( $regions ) ) {
			$metabox->add_field( array(
				'id'      => $prefix . 'region',
				'name'    => esc_html__( 'Club Region', 'judomb' ),
				'type'    => 'select',
				'options' => $regions,
			) );
		}

		/* Instructor group. */
		$instructor_group = $metabox->add_field( array(
			'id'          => $prefix . 'instructor',
			'type'        => 'group',
			'name'        => __( 'Instructor(s)', 'judomb' ),
			'options'     => array(
				'group_title'   => esc_html__( 'Instructor {#}', 'judomb' ), // {#} gets replaced by row number
				'add_button'    => esc_html__( 'Add Instructor', 'judomb' ),
				'remove_button' => esc_html__( 'Remove Instructor', 'judomb' ),
				'sortable'      => true,
			),
		) );

		$metabox->add_group_field( $instructor_group, array(
			'name' => esc_html__( 'Name', 'judomb' ),
			'id'   => 'name',
			'type' => 'text',
		) );

		$metabox->add_group_field( $instructor_group, array(
			'name' => esc_html__( 'Email', 'judomb' ),
			'id'   => 'email',
			'type' => 'text',
		) );
		/* End instructor group. */

		/* Schedule group. */
		$sched_group = $metabox->add_field( array(
			'id'   => $prefix . 'schedule',
			'type' => 'group',
			'name' => esc_html__( 'Schedule', 'judomb' ),
			'options' => array(
				'group_title'    => esc_html__( 'Schedule {#}', 'judomb' ),
				'add_button'     => esc_html__( 'Add Schedule', 'judomb' ),
				'remove_button'  => esc_html__( 'Remove Schedule', 'judomb' ),
				'sortable'       => true,
			),
		) );

		$metabox->add_group_field( $sched_group, array(
			'id'   => 'title',
			'name' => esc_html__( 'Schedule Title', 'judomb' ),
			'type' => 'text',
		) );

		$metabox->add_group_field( $sched_group, array(
			'id'   => 'details',
			'name' => esc_html__( 'Schedule Details', 'judomb' ),
			'type' => 'wysiwyg',
			'options' => array(
				'teeny'         => true,
				'textarea_rows' => 5,
				'media_buttons' => false,
			),
		) );

		$metabox->add_group_field( $sched_group, array(
			'id'   => 'start_date',
			'name' => esc_html__( 'Schedule Start', 'judomb' ),
			'type' => 'text_date_timestamp',
		) );

		$metabox->add_group_field( $sched_group, array(
			'id'   => 'end_date',
			'name' => esc_html__( 'Schedule End', 'judomb' ),
			'type' => 'text_date_timestamp',
		) );

		/* End schedule group. */

		$metabox->add_field( array(
			'id'   => $prefix . 'url',
			'name' => esc_html__( 'Website', 'judomb' ),
			'type' => 'text',
		) );

		$metabox->add_field( array(
			'id'   => $prefix . 'phone',
			'name' => esc_html__( 'Phone #', 'judomb' ),
			'type' => 'text',
			'repeatable' => true,
		) );

		$metabox->add_field( array(
			'id'   => $prefix . 'address',
			'name' => esc_html__( 'Address', 'judomb' ),
			'type' => 'wysiwyg',
			'options' => array(
				'teeny'         => true,
				'textarea_rows' => 5,
				'media_buttons' => false,
			),
		) );

		$metabox->add_field( array(
			'id'   => $prefix . 'notes',
			'name' => esc_html__( 'Notes', 'judomb' ),
			'type' => 'wysiwyg',
			'options' => array(
				'teeny'         => true,
				'media_buttons' => false,
			),
		) );
	}

	/**
	 * Adds any required admin notices in the back end.
	 *
	 * Currently: checks to ensure CMB2 is installed.
	 *
	 * @return void
	 * @since 1.0.0 Introduced.
	 */
	function admin_notices() {
		if ( ! class_exists( 'CMB2' ) ) {
			echo '<div class="notice notice-error"><p>Please install and activate the CMB2 plugin.</p></div>'; // wpcs: xss ok.
		}
	}

	/**
	 * Filters the content.
	 *
	 * @param  string $content The content.
	 * @return string          The filtered content.
	 * @since 1.0.0 Introduced.
	 */
	function content_filter( $content ) {
		if ( is_singular( JudoMB_Clubs::POST_TYPE ) ) {
			global $post;
			$content = JudoMB_Meta_Display::get_club_meta( $post->ID );
		}
		return $content;
	}

	 /**
	  * Crafts the query for the judo_club archive page.
	  *
	  * @param WP_Query $query The query.
	  * @return void
	  * @since 1.0.0 Introduced.
	  */
	function archive_query( $query ) {
		if ( $query->is_post_type_archive( JudoMB_Clubs::POST_TYPE ) ) {
			$query->set( 'orderby',       'title' );
			$query->set( 'order',         'ASC' );
			$query->set( 'posts_per_page', 100 );
		}
	}

	/**
	 * Selects the template file.
	 *
	 * @param  string $template The template file location.
	 * @return string           The filtered template file location.
	 * @since 1.0.0 Introduced.
	 */
	function template_selector( $template ) {
		if ( is_post_type_archive( JudoMB_Clubs::POST_TYPE ) ) {
			$desired_template = plugin_dir_path( __FILE__ ) . '/templates/judo-club-archive.php';
			if ( file_exists( $desired_template ) ) {
				$template = $desired_template;
			}
		}
		return $template;
	}

	/**
	 * Loads the stylesheet and scripts specific to the post type archive page.
	 *
	 * @return void
	 * @since 1.0.0 Introduced.
	 */
	function archive_page_styles_and_scripts() {
		if ( is_post_type_archive( JudoMB_Clubs::POST_TYPE ) ) {
			$handle = JudoMB_Clubs::POST_TYPE . 'archive';
			$src = plugins_url( 'styles/archive-styles.css', __FILE__ );
			$deps = array( 'judo-mb-style' );
			$ver = '1.0.0';
			wp_enqueue_style( $handle, $src, $deps, $ver );

			$handle    = 'judomb-club-filter';
			$src       = plugins_url( 'scripts/club-search-filter.js', __FILE__ );
			$deps      = array( 'jquery' );
			$in_footer = true;
			wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
		}
	}
}

new JudoMB_Clubs;
