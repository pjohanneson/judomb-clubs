<?php
/**
 * Meta Display class file.
 *
 * @package judomb\clubs\meta-display
 */

/**
 * Meta Display class.
 *
 * @since 1.0.0 Introduced.
 */
class JudoMB_Meta_Display {

	/**
	 * Gets the metadata for a club.
	 *
	 * @param  int $id The club ID.
	 * @return string  The metadata string.
	 * @since  1.0.0   Introduced.
	 */
	public static function get_club_meta( $id = 0 ) {
		$id = intval( $id );
		$content = '';
		// Always, always.
		$single = true;
		$dojo_data_fields = array(
			'covid_rtp_level' => array(
				'label'    => __( 'COVID-19 Status', 'judomb' ),
				'callback' => array( 'JudoMB_Meta_Display', 'covid_rtp_level_cb' ),
			),
			'instructor' => array(
				'label'    => __( 'Instructor', 'judomb' ),
				'callback' => array( 'JudoMB_Meta_Display', 'instructor_cb' ),
			),
			'region' => array(
				'label'    => __( 'Region', 'judomb' ),
				'callback' => array( 'JudoMB_Meta_Display', 'region_cb' ),
			),
			'address' => array(
				'label'    => __( 'Address', 'judomb' ),
				'callback' => array( 'JudoMB_Meta_Display', 'address_cb' ),
			),
			'schedule' => array(
				'label'    => __( 'Schedule', 'judomb' ),
				'callback' => array( 'JudoMB_Meta_Display', 'schedule_cb' ),
			),
			'url' => array(
				'label'    => __( 'Club Website', 'judomb' ),
				'callback' => array( 'JudoMB_Meta_Display', 'url_cb' ),
			),
			'phone' => array(
				'label'    => __( 'Phone', 'judomb' ),
				'callback' => array( 'JudoMB_Meta_Display', 'phone_cb' ),
			),
			'notes' => array(
				'label'    => __( 'Notes', 'judomb' ),
				'callback' => array( 'JudoMB_Meta_Display', 'notes_cb' ),
			),
		);
		foreach ( $dojo_data_fields as $field => $data_array ) {
			$data = get_post_meta( $id, JudoMB_Clubs::PREFIX . $field, $single );
			$content .= call_user_func( $data_array['callback'], $data, $data_array['label'] );
		}
		return $content;
	}

	public static function covid_rtp_level_cb( $status, $label = '' ) {
		$status = absint( $status );
		if ( 0 === $status ) {
			return '';
		}
		$content = '';
		$content .= '<p>';
		$content .= '<span class="covid covid-open" title="Phase ' . $status . '">Open</span>';
		$content .= '</p>';
		return $content;
	}

	public static function region_cb( $region, $label = '' ) {
		$content = '';
		if ( empty( $region ) ) {
			return $content;
		}
		$content .= "<h2>$label</h2>\n";
		$content .= $region;
		if ( is_singular() ) {
			$content .= __( "<aside>See <a href='https://www.sportmanitoba.ca/regional-boundaries'>Sport Manitoba's regional boundaries</a> for more information.</aside>", 'judomb' );
		}
		return $content;
	}

	/**
	 * Callback for the 'instructor' metadata.
	 *
	 * @param  array  $instructors The list of instructors.
	 * @param  string $label       The label text.
	 * @return string              The filtered text.
	 * @since  1.0.0               Introduced.
	 */
	public static function instructor_cb( $instructors, $label = '' ) {
		$content = '';
		if ( empty( $instructors ) ) {
			return $content;
		}
		$content .= '<h2>' . _n( 'Instructor', 'Instructors', count( $instructors ), 'judomb' ) . '</h2>' . PHP_EOL;
		$content .= '<ul>' . PHP_EOL;
		foreach ( $instructors as $instructor ) {
			$content .= '<li>';
			if ( ! empty( $instructor['email'] ) ) {
				// @todo Add eae function if available.
				if ( function_exists( 'eae_encode_emails' ) ) {
					$instructor['email'] = eae_encode_emails( $instructor['email'] );
				}
				$content .= "<a href='mailto:{$instructor['email']}'>{$instructor['name']}</a>";
			} else {
				$content .= $instructor['name'];
			}
			$content .= '</li>' . PHP_EOL;
		}
		$content .= '</ul>' . PHP_EOL;
		$content = wpautop( $content );
		return $content;
	}

	/**
	 * Callback for the 'address' metadata.
	 *
	 * @param  string $address The club's address.
	 * @param  string $label   The label text.
	 * @return string          The filtered text.
	 * @since  1.0.0           Introduced.
	 */
	public static function address_cb( $address = '', $label = '' ) {
		$content = '';
		if ( empty( $address ) ) {
			return $content;
		}
		$content .= '<h2>Address</h2>';
		$content .= wpautop( $address );
		return $content;
	}

	/**
	 * Callback for the 'url' metadata.
	 *
	 * @param  string $url   The club's URL(s).
	 * @param  string $label The label text.
	 * @return string        The filtered text.
	 * @since  1.0.0         Introduced.
	 */
	public static function url_cb( $url = '', $label = '' ) {
		$content = '';
		if ( empty( $url ) ) {
			return $content;
		}
		global $post;
		$content .= '<h2>Website</h2>';
		$content .= "<a href='{$url}'>{$post->post_title} Website</a>";
		$content = wpautop( $content );
		return $content;

	}

	/**
	 * Callback for the 'schedule' metadata.
	 *
	 * @param  string $schedules The club's schedule(s).
	 * @param  string $label     The label text.
	 * @return string            The filtered text.
	 * @since  1.0.0             Introduced.
	 */
	public static function schedule_cb( $schedules = array(), $label = '' ) {
		$content = '';
		if ( empty( $schedules ) ) {
			return $content;
		}
		$content .= '<h2>Schedule</h2>';
		$now = current_time( 'timestamp' );
		// Sorts the array, if necessary.
		if ( 1 < count( $schedules ) ) {
			usort( $schedules, array( 'JudoMB_Meta_Display', 'sort_schedule' ) );
		}
		$schedule_found = false;
		foreach ( $schedules as $schedule ) {
			if ( ! $schedule_found ) {
				// Look for schedules with a start date first.
				if ( ! empty( $schedule['start_date'] ) ) {
					if ( $schedule['start_date'] < $now && $schedule['end_date'] > $now ) {
						$schedule_found = true;
						$content .= wpautop( $schedule['details'] );
					}
				}
			}
		}
		if ( ! $schedule_found ) {
			// OK, so they're all in the future, or expired, or unlabeled.
			$schedule = array_pop( $schedules );
			$content .= wpautop( $schedule['details'] );
		}
		$content = wpautop( $content );

		return $content;
	}

	/**
	 * Callback for the 'phone' metadata.
	 *
	 * @param  array  $phones The club's phone number(s).
	 * @param  string $label  The label text.
	 * @return string         The filtered text.
	 * @since  1.0.0          Introduced.
	 */
	public static function phone_cb( $phones = '', $label = '' ) {
		$content = '';
		if ( empty( $phones ) ) {
			return $content;
		}
		$content .= '<h2>' . _n( 'Phone Number', 'Phone Numbers', count( $phones ), 'judomb' ) . '</h2>' . PHP_EOL;
		$content .= '<ul>' . PHP_EOL;
		foreach ( $phones as $phone ) {
			$content .= '<li>';
			if ( wp_is_mobile() ) {
				$content .= "<a href='tel:{$phone}'>{$phone}</a>";
			} else {
				$content .= $phone;
			}
			$content .= '</li>' . PHP_EOL;
		}
		$content .= '</ul>' . PHP_EOL;
		$content = wpautop( $content );

		return $content;
	}

	/**
	 * Callback for the 'notes' metadata.
	 *
	 * @param  string $notes Any notes associated with the club.
	 * @param  string $label The label text.
	 * @return string        The filtered text.
	 * @since  1.0.0         Introduced.
	 */
	public static function notes_cb( $notes = '', $label = '' ) {
		$content = '';
		if ( empty( $notes ) ) {
			return $content;
		}
		$content .= '<h2>Notes</h2>';
		$content .= wpautop( $notes );
		return $content;
	}

	/**
	 * A usort() callback for the schedule list.
	 *
	 * @param array $a  Item A.
	 * @param array $b  Item B.
	 * @return int      The sort result.
	 * @since 1.0.0     Introduced.
	 */
	public static function sort_schedule( $a, $b ) {
		if ( $a['start_date'] > $b['start_date'] ) {
			return 1;
		} else if ( $a['start_date'] < $b['start_date'] ) {
			return -1;
		} else if ( $a['start_date'] === $b['start_date'] ) {
			if ( $a['end_date'] > $b['end_date'] ) {
				return 1;
			} else if ( $a['end_date'] < $b['end_date'] ) {
				return -1;
			}
		}
		return 0;
	}

}
