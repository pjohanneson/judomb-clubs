<?php
/**
 * Archive template file for the judo_club custom post type.
 *
 * @package judomb\clubs
 * @since   1.0.0
 */

get_header();

if ( have_posts() ) {
	echo '<form>' . PHP_EOL;
	echo '<p><input type="text" id="filter_text" placeholder="Type to search"/></p>' . PHP_EOL; // wpcs: xss ok.
	echo '<div class="judo-club-list">';
	while ( have_posts() ) {
		the_post();
		$id = get_the_ID();
		echo sprintf( '<div class="judo-club" id="judo-club-%d">', $id ); // wpcs: xss ok.
		the_title( '<h1><a href="' . get_permalink( $id ) . '">', '</a></h1>' );
		echo JudoMB_Meta_Display::get_club_meta( $id ); // wpcs: xss ok.
		echo '</div> <!-- .judo-club #judo-club-' . $id . ' -->'; // wpcs: xss ok.
	}
	echo '</div><!-- .judo-club-list -->' . PHP_EOL;
	echo '</form>' . PHP_EOL;
}

get_sidebar();
get_footer();
