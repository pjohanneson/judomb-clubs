<?php
/**
 * Plugin loader file.
 *
 * @package judomb\clubs
 */

/**
 * Plugin Name: Judo MB Clubs
 * Description: Management for Judo Manitoba clubs.
 * Author Name: Patrick Johanneson
 * Author URI:  https://patrickjohanneson.com/
 * Version:     1.1.0
 */

require 'class-judomb-clubs.php';
require 'class-judomb-meta-display.php';

add_action( 'wp_enqueue_scripts', 'fr_load_up_those_covid_styles_2232' );
function fr_load_up_those_covid_styles_2232() {
	wp_enqueue_style( 'judomb-clubs-covid', plugins_url( 'styles/covid.css', __FILE__ ) );
}