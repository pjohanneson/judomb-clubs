// Search filter 
jQuery(document).ready(function($) {
    $("#filter_text").keyup(function () {
        var filter_text = this.value;
        $('.judo-club').each( function() {
            if ( $(this).text().toUpperCase().indexOf(filter_text.toUpperCase() ) != -1 ) {
                $(this).show( 300 );
            } else {
                $(this).hide( 300 );
            }
        });
    }); 
});